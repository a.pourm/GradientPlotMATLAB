%GRADIENTPLOT() Create scatter and normal plots with gradient color on the
%             vertical direction
%
%   gradientplot( XDATA , YDATA , OPTIONS ) Accepts XDATA as vector of
%   length N and YDATA as a matrix of length M-by-N to plot the requested
%   scatter or line plot
%
%   COLORINTENSITY = gradientplot( XDATA , YDATA , OPTIONS ) Accepts XDATA 
%   as vector of length N and YDATA as a matrix of length M-by-N to plot 
%   the requested scatter or line plot returning color intensity values
%
%
%   Input Arguments:
%       XDATA
%       TYPE: vector | SIZE: N
%           This data represents the x-axis values for the plot. The number
%           of element, N, should be equal to the number of columnc in
%           YDATA.
%
%       YDATA
%       TYPE: matrix | SIZE: M-by-N
%           This is the actual M time series with length of N. The color
%           intensity will be calculated based on this input and other
%           specifications.
%       
%       OPTIONS
%       TYPE: struct | SIZE: N/A
%           This is the general structure containing all the settings that
%           are allowed. Different settings and their applications are
%           described below.
%
%           'colormap'
%           TYPE: char or numeric | OPTIONS: check out below
%               To select a colormap from exisiting list of MATLAB, or
%               specify a different one, DEFAULT = 'spring'. The available
%               list of colormap in MATLAB is 'parula' , 'jet' , 'hsv' , 
%               'hot' , 'cool' , 'spring' , 'summer' , 'autumn' , 'winter' 
%               , 'gray' , 'bone' , 'copper' , 'pink' , 'lines' , 
%               'colorcube' , 'prism' , 'flag' , and 'white'.
%
%           'colorstyle'
%           TYPE: char | OPTOINS: 'continous' , 'discrete' 
%               For continuous gradient colors, select 'continuous'; for a
%               scatter/plot with specific number of colors, select
%               'discrete', DEFAULT='continuous'
%
%           'colorstepno'
%           TYPE: numeric | OPTIONS: any positive integer
%               When 'colorstyle' is set to 'discrete', the number of
%               colors can be selected by this field, DEFAULT=5.
%
%           'intensestyle'
%           TYPE: char | OPTIONS: 'global' , 'local' , 'custom'
%               Color intensity is calculated based on the magnitude of
%               YDATA values. When 'global' is selected, intensity will be
%               calculated based on the global minimum and maximum values.
%               When 'local' is selected, intensity is calculated based on
%               each time in the series separately. The maximum and minimum
%               is normalized based on the global maximum and minimum for
%               better color representation. When 'custom' is selected, the
%               set of color intensity given by the users will be used.
%               This is useful for time series plot with different 
%               probability. 
%
%           'customintval'
%           TYPE: double | OPTIONS: N/A
%               It should be initiated when 'intensestyle' is set to
%               'custom'. It should be the same size as YDATA (N-by-M).
%               Be careful about this option as the element should vary
%               reasonaly by the correspondant values in YDATA.
%
%           'midpoint'
%           TYPE: char | OPTIONS: 'none' , 'mean' , 'median' , 'custom'
%               This option allows user to specify the beginning of the
%               gradient color from certain points to extend to both up and 
%               down in Y direction. If 'none', there will start from the 
%               lowest levle of YDATA. 'mean' and 'median' automatically 
%               find mean and median of the YDATA to start from there. 
%               'custom' allows user to specify a set of points manually. 
%
%           'custommidval'
%           TYPE: numeric vector | OPTIONS: N/A
%               This is mandatory if 'midpoint' is set to 'custom'. It
%               should be vector with the same length as XDATA.
%           
%           'jitter'
%           TYPE: logical | OPTIONS: true , false
%               This is a capability in scatter plot to avoid data
%               opverlapping, and to make a cloud of data. When it is set
%               to on, it will create a distance between cluster of data
%               within a time instance, DEFAULT = false.
%
%           'jitteramount'
%           TYPE: double | OPTIONS: any number between [0,1]
%               When 'jitter' is on, it can be set to make distance between
%               clusters, DEFAULT = 0.5.
%
%           'filledmarker'
%           TYPE: logical | OPTIONS: on , off
%               When 'linestyle' is set to '.', this switch can be used to
%               fill the market in a scatter plot, DEFAULT = on.
%
%           'markerfacealpha'
%           TYPE: double | OPTIONS: any number between [0,1]
%               This value is used to tranparent the marker filled color
%               when 'linestyle' is set to '-', DEFAULT = 0.9.
%
%           'markeredgealpha'
%           TYPE: double | OPTIONS: any number between [0,1]
%               This value is used to tranparent the marker edge color
%               when 'linestyle' is set to '-', DEFAULT = 0.9.
%
%           'showcolorbar'
%           TYPE: logical | OPTIONS: on , off
%               To show the colorbar along the y-axis, DEFAULT = off.
%
%           'colorbarlabel'
%           TYPE: char | OPTIONS: any text
%               To assign a label to the colorbar when 'showcolorbar' is
%               set to on, DEFAULT = [].
%
%           'colorbarfontsize'
%           TYPE: double | OPTIONS: any number
%               To set a certain font size for the colorbar label when
%               'showcolorbar' is set to on, DEFAULT = 12.
%
%   To run this program, you do not need to install any specific MATLAB toolbox
%
%
% Copyright (c) 2018,  Seyyed Ali Pourmousavi Kani <a.pour@uq.edu.au>
% All rights reserved.
%
% Redistribution and use in source and binary forms, with or without
% modification, are permitted provided that the following conditions are
% met:
%     * Redistributions of source code must retain the above copyright
%       notice, this list of conditions and the following disclaimer.
%     * Redistributions in binary form must reproduce the above copyright
%       notice, this list of conditions and the following disclaimer in
%       the documentation and/or other materials provided with the distribution
%
% THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
% AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
% IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
% ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
% LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
% CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
% SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
% INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
% CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
% ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
% POSSIBILITY OF SUCH DAMAGE.


function COLORINTENSITYVALUES = gradientplot( XDATA , YDATA , OPTIONS )

%% PREPARING PARAMETERS

% checking XDATA and YDATA
if ~isvector(XDATA)
    error('Error: XDATA should be a vector.');
elseif ~ismatrix(YDATA) || length(XDATA) ~= size(YDATA , 2)
    error('Error: YDATA should be matrix with the same number of columns as in XDATA');
end

% 'colormap'
ACCEPTABLEINP = {'default' , 'parula' , 'jet' , 'hsv' , ...
                 'hot' , 'cool' , 'spring' , 'summer' , ...
                 'autumn' , 'winter' , 'gray' , 'bone' , ...
                 'copper' , 'pink' , 'lines' , 'colorcube' , ...
                 'prism' , 'flag' , 'white'};
             
if ischar(OPTIONS.colormap)
    COLORMAP = PREPINP(OPTIONS , 'colormap' , @ischar , 'spring' , ACCEPTABLEINP);
elseif ismatrix(OPTIONS.colormap) && ...
       size(OPTIONS.colormap , 2) == 3 && ...
       sum(find(OPTIONS.colormap > 1) + find(OPTIONS.colormap < 0)) == 0
    COLORMAP = OPTIONS.colormap;
else
    error(['Error: ''colormap'' should either be an existing MATLAB colormap' , ...
        ' or be a numeric matrix with at least three rows and three columns.' , ...
        ' Values should be less than 1 and bigger than 0. ']);
end

% 'colorstyle'
ACCEPTABLEINP = {'default' , 'continuous' , 'discrete'};
COLORSTYLE = PREPINP(OPTIONS , 'colorstyle' , @ischar , 'continuous' , ACCEPTABLEINP);

% 'colorstepno'
COLORSTEPNO = [];
if strcmp(COLORSTYLE , 'discrete')
    ACCEPTABLEINP = {'default' , '5' , 5};
    COLORSTEPNO = PREPINP(OPTIONS , 'colorstepno' , @isnumeric , 5 , ACCEPTABLEINP);
end

% 'intensestyle'
ACCEPTABLEINP = {'default' , 'global' , 'local' , 'custom'};
INTENSESTYLE = PREPINP(OPTIONS , 'intensestyle' , @ischar , 'global' , ACCEPTABLEINP);

% 'customintval'
if (strcmp(INTENSESTYLE , 'custom') && isempty(OPTIONS.customintval)) || ...
    (strcmp(INTENSESTYLE , 'custom') && ~ismatrix(OPTIONS.customintval)) || ...
    (strcmp(INTENSESTYLE , 'custom') && (size(YDATA , 1) ~= size(OPTIONS.customintval , 1) || ...
    size(YDATA , 2) ~= size(OPTIONS.customintval , 2)))
    error(['Error: ''customintval'' should be given for ''intensestyle''' , ...
           'of type ''custom''. The size should be the same as YDATA.']);
elseif strcmp(INTENSESTYLE , 'custom')
    COLORINTENSITY = OPTIONS.customintval;
else
    OPTIONS.customintval = [];
end

% 'midpoint'
MIDPOINT = [];
if ~isempty(OPTIONS.customintval)
    MIDPOINT = 'customintval'; % nothing to do with midpoint as the color intensity is given directly
elseif isempty(OPTIONS.('midpoint'))
    error('Error: ''''midpoint'' is mandatory to be defined.');
elseif strcmp(OPTIONS.('midpoint') , 'custom')
    if (length(OPTIONS.('custommidval')) ~= size(YDATA , 2)) || ...
            ~isvector(OPTIONS.('custommidval')) || ...
            ~isnumeric(OPTIONS.('custommidval'))
        error(['Error: In ''midpoint'' of type ''custom''', ...
               ' length of ''custommidval'' should be equal to' , ...
               ' the number of columns in given data. It should also be numeric.']);
    else
        MIDPOINTVAL = OPTIONS.('midpoint');
        MIDPOINT = 'custom';
    end
else
    ACCEPTABLEINP = {'default' , 'none' , 'mean' , 'median'};
    MIDPOINT = PREPINP(OPTIONS , 'midpoint' , @ischar , 'mean' , ACCEPTABLEINP);
end     % END OF isempty(OPTIONS.('midpoint'))

% 'linestyle'
ACCEPTABLEINP = {'default' , '.' , '-'};
LINESTYLE = PREPINP(OPTIONS , 'linestyle' , @ischar , '-' , ACCEPTABLEINP);

% 'jitter' 
if isfield(OPTIONS , 'jitter') 
    if ~isempty(OPTIONS.('jitter'))
        if strcmp(LINESTYLE , '.') && OPTIONS.jitter
            JITTER = true;
        else
            JITTER = false;
        end
    else
        JITTER = false;
    end
else
    JITTER = false;
end

% 'jitteramount'
if isfield(OPTIONS , 'jitteramount') && JITTER
    if ~isempty(OPTIONS.jitteramount) 
        if isnumeric(OPTIONS.jitteramount)
            JITTERAMOUNT = OPTIONS.jitteramount;
        else
            JITTERAMOUNT = 0.5;
        end
    else
        JITTERAMOUNT = [];
    end
else
    JITTERAMOUNT = [];
end

% 'filledmarker'
if isfield(OPTIONS , 'filledmarker')
    if strcmp(LINESTYLE , '.') && ~isempty(OPTIONS.filledmarker)
        if OPTIONS.filledmarker
            FILLEDMARKER = true;
        else
            FILLEDMARKER = false;
        end
    else
        FILLEDMARKER = false;
    end
else
    FILLEDMARKER = false;
end

% 'markerfacealpha' 
if isfield(OPTIONS , 'markerfacealpha')
    if ~isempty(OPTIONS.markerfacealpha)
        if FILLEDMARKER && isnumeric(OPTIONS.markerfacealpha) && ...
                OPTIONS.markerfacealpha > 0.01 && OPTIONS.markerfacealpha < 1.0
            MARKERFACEALPHA = OPTIONS.markerfacealpha;
        else
            MARKERFACEALPHA = 0.9;
        end
    else
        MARKERFACEALPHA = 0.9;
    end
else
    MARKERFACEALPHA = 0.9;
end

% 'markeredgealpha'
if isfield(OPTIONS , 'markeredgealpha')
    if ~isempty(OPTIONS.markeredgealpha)
        if FILLEDMARKER && isnumeric(OPTIONS.markeredgealpha) && ...
                OPTIONS.markeredgealpha > 0.01 && OPTIONS.markeredgealpha < 1.0
            MARKEREDGEALPHA = OPTIONS.markeredgealpha;
        else
            MARKEREDGEALPHA = 0.9;
        end
    else
        MARKEREDGEALPHA = 0.9;
    end
else
    MARKEREDGEALPHA = 0.9;
end

% 'showcolorbar' , 'colorbarlabel' , 'colorbarfontsize'
if isfield(OPTIONS , 'showcolorbar') && OPTIONS.showcolorbar
    SHOWCOLORBAR = true;
    if isfield(OPTIONS , 'colorbarlabel') && ...
        ~isempty(OPTIONS.colorbarlabel)  && ...
        ischar(OPTIONS.colorbarlabel)
        COLORBARLABEL = OPTIONS.colorbarlabel;
    else
        COLORBARLABEL = [];
    end
    if isfield(OPTIONS , 'colorbarfontsize') && OPTIONS.colorbarfontsize
        COLORBARFONTSIZE = OPTIONS.colorbarfontsize;
    else
        COLORBARFONTSIZE = [];
    end
else
    SHOWCOLORBAR = false;
end
    


%% PREPARING INTERNAL PARAMETERS
if strcmp(MIDPOINT , 'mean') || ...
    strcmp(MIDPOINT , 'median')
    
    MIDPOINTVAL = feval(MIDPOINT , YDATA);

end

% producing COLORINTENSITY
switch MIDPOINT
    case 'none'
        %-----------------------------------------------------------------
        switch INTENSESTYLE
            case 'global'

                COLORINTENSITY = YDATA ./ max(max(YDATA));

            case {'local' , 'custom'}

                MAXCOLORINTENSITY = max(YDATA , [] , 1);
                MINCOLORINTENSITY = min(YDATA , [] , 1);

                COLORINTENSITY = zeros(size(YDATA));
                for j = 1:size(YDATA , 1)

                    COLORINTENSITY(j , :) = (YDATA(j , :) - ...
                                    MINCOLORINTENSITY) ./ ...
                                   (MAXCOLORINTENSITY - MINCOLORINTENSITY);

                end     % END OF j = 1:size(YDATA , 1)
                
        end     % END OF LINESTYLE
        MAXMIN = [min(min(YDATA)) , max(max(YDATA))];
        
    case {'mean' , 'median' , 'custom'}
        %-----------------------------------------------------------------
        switch INTENSESTYLE
            case 'global'
                
                [COLORINTENSITY , MAXMIN]= COLORRESCALING(YDATA , MIDPOINTVAL , INTENSESTYLE);

            case {'local' , 'custom'}

                [COLORINTENSITY , MAXMIN] = COLORRESCALING(YDATA , MIDPOINTVAL , INTENSESTYLE);
                
        end     % END OF INTENSESTYLE        
        
end     % END OF MIDPOINT


%% PLOTTING
figure; hold on;

if strcmp(LINESTYLE , '.')  % scatter plot
    
    h = scatter(repmat(XDATA(:) , size(YDATA , 1) , 1) , ...
            reshape(YDATA' , size(YDATA , 1) * size(YDATA , 2) , 1) , ...
            10 , reshape(COLORINTENSITY' , size(YDATA , 1) * size(YDATA , 2) , 1));
        
    
    if JITTER
        set(h, 'jitter' , 'on');
    end
    if ~isempty(JITTERAMOUNT)
        set(h , 'jitterAmount' , JITTERAMOUNT);
    end
    if FILLEDMARKER
        set(h, 'MarkerFaceColor' , 'flat' , ...
               'MarkerFaceAlpha' , MARKERFACEALPHA , ...
               'MarkerEdgeAlpha' , MARKEREDGEALPHA);
    end
    
else    % line plot
    Z = zeros(size(XDATA));
    for i = 1:size(YDATA , 1)

        surface([XDATA ; XDATA] , ...
                [YDATA(i , :) ; YDATA(i , :)] , ...
                [Z ; Z] , [COLORINTENSITY(i , :) ; COLORINTENSITY(i , :)] , ...
                'facecol' , 'no' , ...
                'edgecol' , 'interp' , ...
                'linew'   , 0.5);

    end  % END OF i = 1:size(YDATA , 1)

end

%% changing colormap as given
if strcmp(COLORSTYLE , 'discrete')
    colormap([COLORMAP , '(' , num2str(COLORSTEPNO) , ')']);
else
    colormap(COLORMAP);
end

if SHOWCOLORBAR
    CB = colorbar;
    
    if ~isempty(COLORBARLABEL)
        CB.Label.String = COLORBARLABEL;
    end
    if ~isempty(COLORBARFONTSIZE)
        CB.Label.FontSize = COLORBARFONTSIZE;
    end
    
    % resetting colormap tick marks
    CB.Limits = MAXMIN;
end

if nargout == 1
    COLORINTENSITYVALUES = COLORINTENSITY;
else
    COLORINTENSITYVALUES = [];
end


end     % END OF gradientplot()


%% ------------------------------------------------------------------------
%  PREPINP() Preparing input parameters
function VAR = PREPINP(OPTIONS , INPNAME , INPTYPE , INPDEFAULT , ACCEPTABLEINP)
    %   OPTIONS         the structure containing all given options
    %   INPNAME         name of the input parameter to be evaluated
    %   INPTYPE         type of the input parameter as a function handle
    %   INPDEFAULT      default value of the given parameter
    %   ACCEPTABLEINP   cellarray containing acceptable values
    
                      
    OPTIONSNAMES = lower(fieldnames(OPTIONS));

    if sum(ismember(INPNAME , OPTIONSNAMES)) == 1
        
        if ~isempty(OPTIONS.(INPNAME)) 
            
            if INPTYPE(OPTIONS.(INPNAME)) 
                
                if isequal(INPTYPE , @isnumeric)  % for numeric parameters
                    if isscalar(OPTIONS.(INPNAME))
                        VAR = lower(OPTIONS.(INPNAME));
                    else
                        error('Error: ''%s'' should be a scalar.' , INPNAME);
                    end
                else % for character parameters
                    if sum(ismember(lower(OPTIONS.(INPNAME)) , ACCEPTABLEINP)) == 1
                        if strcmpi(OPTIONS.(INPNAME) , 'default')
                            VAR = INPDEFAULT;
                        else
                            VAR = lower(OPTIONS.(INPNAME));
                        end
                    else
                        tmp = char(INPTYPE);

                        TMPLIST = [];
                        for i = 1:length(ACCEPTABLEINP)
                            TMPLIST = [TMPLIST , sprintf(', ''%s''', char(ACCEPTABLEINP(i)))];
                        end
                
                        error('Error: %s should be of type %s, and one of the listed values %s' , ...
                            INPNAME , tmp(3:end) , TMPLIST);
                    end  
                end     % END OF isequal(INPTYPE , @isnumeric)
            else
                tmp = char(INPTYPE);
                
                TMPLIST = [];
                for i = 1:length(ACCEPTABLEINP)
                    TMPLIST = [TMPLIST , sprintf(', ''%s''', char(ACCEPTABLEINP(i)))];
                end
                
                error('Error: %s should be of type %s, and one of the listed values %s' , ...
                    INPNAME , tmp(3:end) , TMPLIST);
            end
            
        else    
            
            VAR = INPDEFAULT;
            
        end     % END OF ~isempty(OPTIONS.(INPNAME)) 
    else
        
        VAR = INPDEFAULT;
        
    end     % END OF sum(ismember(INPNAME , OPTIONSNAMES)) == 1
    
    if isnumeric(VAR)
        fprintf('''%s'' = %s\n' , INPNAME , str2double(VAR));
    else
        fprintf('''%s'' = %s\n' , INPNAME , VAR);
    end

end     % END OF PREPINP()



%% ------------------------------------------------------------------------
%  COLORRESCALING() Rescaling color intensity data to have normalised
%  colors
function [COLORINTENSITY , MAXMIN]= COLORRESCALING(YDATA , MIDPOINTVAL , INTENSESTYLE)
    
    switch INTENSESTYLE
        case 'global'
            
            tmpIND = YDATA > MIDPOINTVAL;
            MAXUP = max(YDATA(tmpIND) , [] , 1);
            MINUP = min(YDATA(tmpIND) , [] , 1);
            MAXofMAX_MINUP = max(MAXUP - MINUP); % the largest difference UP
            
            tmpIND = YDATA <= MIDPOINTVAL;
            MAXDOWN = min(YDATA(tmpIND) , [] , 1);
            MINDOWN = max(YDATA(tmpIND) , [] , 1);
            MAXofMAX_MINDOWN = max(abs(MAXDOWN - MINDOWN)); % the largest different DOWN
            
            MAXofALL = max(MAXofMAX_MINUP , MAXofMAX_MINDOWN); % the largest of the two groups
            
%             COLORINTENSITY = YDATA ./ MAXofALL;
            COLORINTENSITY = zeros(size(YDATA));
               
            tmpIND = YDATA > MIDPOINTVAL;
            COLORINTENSITY(tmpIND) = (YDATA(tmpIND) - MINUP) ./ ...
                    (MAXUP - MINUP) .* MAXofALL;

            tmpIND = YDATA <= MIDPOINTVAL;
            COLORINTENSITY(tmpIND) = abs(YDATA(tmpIND) - MINDOWN) ./ ...
                    (MINDOWN - MAXDOWN) .* MAXofALL;
            
            % for colormap tick marks
            MAXMIN = [min(min(YDATA)) , max(max(YDATA))]; 
            
        case 'local'
            
            MAXUP = zeros(size(YDATA , 2) , 1);
            MINUP = zeros(size(YDATA , 2) , 1);
            MAXofMAX_MINUP = zeros(size(YDATA , 2) , 1);
            MAXDOWN = zeros(size(YDATA , 2) , 1);
            MINDOWN = zeros(size(YDATA , 2) , 1);
            MAXofMAX_MINDOWN = zeros(size(YDATA , 2) , 1);
            COLORINTENSITY = zeros(size(YDATA));
            
            for j = 1:size(YDATA , 2)
                tmpIND = YDATA(: , j) > MIDPOINTVAL(j);
                MAXUP(j) = max(YDATA(tmpIND , j));
                MINUP(j) = min(YDATA(tmpIND , j));
                MAXofMAX_MINUP(j) = max(MAXUP(j) - ...
                    MINUP(j)); % the largest difference UP
            
                tmpIND = YDATA(: , j) <= MIDPOINTVAL(j);
                MAXDOWN(j) = min(YDATA(tmpIND , j));
                MINDOWN(j) = max(YDATA(tmpIND , j));
                MAXofMAX_MINDOWN(j) = max(abs(MAXDOWN(j) - ...
                    MINDOWN(j))); % the largest different DOWN
            end
            
            MAXofALL = max(max(MAXofMAX_MINUP) , ...
                           max(MAXofMAX_MINDOWN)); % the largest of the two groups
            
            COLORINTENSITY = zeros(size(YDATA));
            for j = 1:size(YDATA , 2)
               
                tmpIND = YDATA(: , j) > MIDPOINTVAL(j);
                COLORINTENSITY(tmpIND , j) = (YDATA(tmpIND , j) - MINUP(j)) ./ ...
                        (MAXUP(j) - MINUP(j)) .* MAXofALL;
                
                tmpIND = YDATA(: , j) <= MIDPOINTVAL(j);
                COLORINTENSITY(tmpIND , j) = abs(YDATA(tmpIND , j) - MINDOWN(j)) ./ ...
                        (MINDOWN(j) - MAXDOWN(j)) .* MAXofALL;
                
            end
            
    end     % END OF INTENSESTYLE
    
end     % END OF COLORRESCALING()
